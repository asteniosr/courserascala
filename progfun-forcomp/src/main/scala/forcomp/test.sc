package forcomp

import Anagrams._

object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val sentence = List("Yes", "man")               //> sentence  : List[java.lang.String] = List(Yes, man)
  val sentence2 = List("married")                 //> sentence2  : List[java.lang.String] = List(married)
  
  val sentOcc = sentenceOccurrences(sentence)     //> sentOcc  : forcomp.Anagrams.Occurrences = List((a,1), (e,1), (m,1), (n,1), (
                                                  //| s,1), (y,1))
  
  //combinations(sentOcc)

	
	//test(sentOcc)



  def sentenceAnagrams2(sentence: Sentence): List[Sentence] = {
	  def test(occ: Occurrences): List[Sentence] = {
			if (occ.isEmpty) List(Nil)
			else	for {
				comb <- combinations(occ) // NO, hayy que trabajar sobre occ y a partir de ahi coger palabras
				word <- dictionaryByOccurrences getOrElse (comb, Nil)
				sent <- test(subtract(occ, wordOccurrences(word)))
			} yield word::sent
		}
  	test(sentenceOccurrences(sentence))
  }                                               //> sentenceAnagrams2: (sentence: forcomp.Anagrams.Sentence)List[forcomp.Anagram
                                                  //| s.Sentence]
  sentenceAnagrams2(sentence)                     //> res0: List[forcomp.Anagrams.Sentence] = List(List(en, as, my), List(en, my, 
                                                  //| as), List(man, yes), List(men, say), List(as, en, my), List(as, my, en), Lis
                                                  //| t(sane, my), List(Sean, my), List(my, en, as), List(my, as, en), List(my, sa
                                                  //| ne), List(my, Sean), List(say, men), List(yes, man))
  
         
  //def candidates(s: Sentence): List[List[Occurrences]] = ???
    
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = {
    def iter(occurrences: Occurrences): List[Sentence] = {
      if (occurrences.isEmpty) List(Nil)
      else for {
        combination <- combinations( occurrences )
        word <- dictionaryByOccurrences getOrElse (combination, Nil)
        sentence <- iter( subtract(occurrences,wordOccurrences(word)) )
        if !combination.isEmpty
      } yield word :: sentence
    }

    iter( sentenceOccurrences(sentence) )
  }                                               //> sentenceAnagrams: (sentence: forcomp.Anagrams.Sentence)List[forcomp.Anagram
                                                  //| s.Sentence]
   
  
  sentenceAnagrams(sentence)                      //> res1: List[forcomp.Anagrams.Sentence] = List(List(en, as, my), List(en, my,
                                                  //|  as), List(man, yes), List(men, say), List(as, en, my), List(as, my, en), L
                                                  //| ist(sane, my), List(Sean, my), List(my, en, as), List(my, as, en), List(my,
                                                  //|  sane), List(my, Sean), List(say, men), List(yes, man))
  
   /*
  def sum(l: Occurrences): Int = l match {
  	case Nil => 0
  	case head::tail => head._2 + sum(tail)
  }
  //combinations(sentOcc) filter (l => sum(l) == sum(sentOcc))

	def encode(occu: Occurrences): List[Sentence] =
    if (occu.isEmpty) List(List())
    else {
      for {
        occ <- occu // iterate over the number
        ann <- showAnagrams(List(occ)) // get the word before the spilt
        rest <- encode(subtract(occu, List(occ))) //get the words after the split
      } yield ann :: rest // join the results
 }
 
 */
 
	//encode(sentenceOccurrences(sentence))

}