package funsets

object Main extends App {
  import FunSets._
  
  val s1 = union(singletonSet(1), union(singletonSet(7), union(singletonSet(3), singletonSet(5))));
  
  printSet(s1);
  
  val s = map(s1, x => x * 5);
  
  printSet(s);
}
