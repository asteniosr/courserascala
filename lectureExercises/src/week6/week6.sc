package week6

object week6 {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val i = 1+1                                     //> i  : Int = 2

	type Word = String
	
	val w = "aloha"                           //> w  : java.lang.String = aloha
	
	val l = (w groupBy (identity) mapValues (rep => rep.length) toList) sortBy (_._1)
                                                  //> l  : List[(Char, Int)] = List((a,2), (h,1), (l,1), (o,1))

	val r = List(('a',1), ('l',1))            //> r  : List[(Char, Int)] = List((a,1), (l,1))
	
	l.foldLeft (r) ((r,c) => r)               //> res0: List[(Char, Int)] = List((a,1), (l,1))
  
  def substract(l: List[(Char, Int)], c: (Char, Int)): List[(Char, Int)] =
   for {
    x <- l
  } yield if (x._1 == c._1) (x._1, x._2 - c._2) else x
                                                  //> substract: (l: List[(Char, Int)], c: (Char, Int))List[(Char, Int)]
  
  val ss = r.foldLeft (l) ((a,c) => substract(a,c)).filter (x => x._2 > 0)
                                                  //> ss  : List[(Char, Int)] = List((a,1), (h,1), (o,1))
  
  type Sentence = List[Word]
  type Occurrences = List[(Char, Int)]
  val abba = List(('a', 2), ('b', 2))             //> abba  : List[(Char, Int)] = List((a,2), (b,2))
     
  def combinations(occurrences: Occurrences): List[Occurrences] = occurrences match {
	  case Nil => List(Nil)
    case head::tail =>
	    for {
	      comb <- combinations(tail)
	      index <- 0 to head._2
	    } yield ((head._1, index) :: comb).filter (x => x._2 > 0)
	}                                         //> combinations: (occurrences: week6.week6.Occurrences)List[week6.week6.Occurre
                                                  //| nces]
 
  combinations(abba)                              //> res1: List[week6.week6.Occurrences] = List(List(), List((a,1)), List((a,2)),
                                                  //|  List((b,1)), List((a,1), (b,1)), List((a,2), (b,1)), List((b,2)), List((a,1
                                                  //| ), (b,2)), List((a,2), (b,2)))

}