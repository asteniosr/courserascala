package week3

object intSets extends App {
	val s1 = new NonEmpty(3, new Empty, new Empty)
	val s2 = s1 incl 4 // TODO not working
}

abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
}

class Empty extends IntSet {
  def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
  def contains(x: Int): Boolean = false
  
  override def toString = "."
}

class NonEmpty (root: Int, left: IntSet, right: IntSet) extends IntSet {
  def incl(x: Int): IntSet = {
    if (x < root) new NonEmpty(root, left incl x, right)
    if (x > root) new NonEmpty(root, left, right incl x)
    this
  }

  def contains(x: Int): Boolean = {
    if (x < root) left contains x
    if (x > root) right contains x
    true 
  }
  
  override def toString = "{" + left + root + right + "}"
}