package week3

object rationals extends App{
  val x = new Rational(10, 30)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)
  
  y + y
  x - y - z
  
  new Rational(2)
}

class Rational (num: Int, den: Int){
  require(den != 0, "denominator must be nonzero")
  
  def this(x: Int) = this(x, 1)
  
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  
  def numerator = num
  def denominator = den
  
  override def toString = {
    val reduced = this.reduce()
    reduced.numerator + "/" + reduced.denominator
  }
  
  def unary_- () =
    new Rational(-numerator, denominator)
  
  def reduce() =
    new Rational(numerator / gcd(numerator, denominator), denominator / gcd(numerator, denominator))
  
  def + (that: Rational) = 
    new Rational(numerator * that.denominator + denominator * that.numerator, denominator * that.denominator)
  
  def - (that: Rational) =
    this + -that
}